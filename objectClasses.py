import os
import sys
from functools import partial
import PySide2.QtWidgets as qw
import PySide2.QtGui as qg
import PySide2.QtCore as qc
from . import style as st
from . import icons as ic
from . import widgets as wd
from . import data as dt

def blank(*args, **kwargs):
    print("Placeholder function.", kwargs)

class Item(wd.BaseWidget):
    '''
    Items that holds text, checklist and other things.
    '''
    def __init__(self, parent=None, title=None, data=None, createDefault=False, *args, **kwargs):
        super(Item, self).__init__(parent=parent, collapsable=True)
        self.title = title or "New Item"
        self.createDefault = createDefault
        self.content = list() # Contains the item's element in order
        # Style
        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), qg.QColor(45,45,45,255))
        self.setPalette(p)
        self.setSizePolicy(qw.QSizePolicy.Expanding, qw.QSizePolicy.Fixed)
        self.setStyleSheet(st.globalStyle)
        self.createAddMenu()
        self.createElements()
        self.createContent()

    def isEmpty(self):
        return self.contentLayout.isEmpty()

    def deleteElement(self, element):
        # if hasattr(element, "getState"):
        #     state = element.getState()
        #     self.parent().parent().undoQueue.append(state)
        #     print("Store in undo queue")
        #     print(self.parent().parent().un)
        self.contentLayout.removeWidget(element)
        element.deleteLater()

    def getState(self):
        '''
        Returns a dictionnary containing the data of the item and its element.
        '''
        state = super().getState()
        state["color"] = self.colorBar.color
        items = list()
        count = self.contentLayout.count()
        for i in range(count):
            item = self.contentLayout.itemAt(i).widget()
            if hasattr(item, "getState"):
                items.append(item.getState())
        state["elements"] = items
        # print("Item", state)
        return state

    def restoreState(self, data):
        super().restoreState(data)
        self.colorBar.setColor(data["color"])
        # Recreate item's elements
        for element in data["elements"]:
            thing = self.addElement(getattr(wd, element["type"]))
            if hasattr(thing, "restoreState"):
                thing.restoreState(element)

    def addElement(self, func, *args, **kwargs):
        element = func(*args, **kwargs)
        self.contentLayout.insertWidget(-2, element, 0)
        if hasattr(element, "closeButton"):
            element.closeButton.clicked.connect(partial(self.deleteElement, element))
        return element

    def createAddMenu(self):
        '''
        Popup menu for the add element button. Options to add all types of content.
        '''
        menu = qw.QMenu(self)
        menu.addAction("Text", partial(self.addElement, wd.Text))
        menu.addAction("Checklist", partial(self.addElement, wd.CheckList))
        menu.addAction("Progress Bar",
                        partial(self.addElement,
                                wd.ProgressBar,
                                progress=77))
        menu.addAction("Image",
                        partial(self.addElement,
                                wd.Image))
        menu.addAction("Gallery", blank)
        menu.addAction("Deadline", blank)
        menu.addAction("Tag", partial(self.addElement, wd.TagBar))
        return menu

    def optionMenu(self):
        menu = qw.QMenu(self)
        addMenu = self.createAddMenu()
        subMenu = menu.addMenu(addMenu)
        subMenu.setText("Add")
        
        menu.addAction("Toggle Title", partial(self.setTitleVisibility, toggle=True))
        return menu

    def createContent(self, createDefault=False, *args, **kwargs):
        # Elements
        if self.createDefault:
            self.descText = wd.Text()
            self.contentLayout.addWidget(self.descText)
        # Layout Management
        self.endStretch = qw.QSpacerItem(0,0, qw.QSizePolicy.Expanding, qw.QSizePolicy.Expanding)
        self.contentLayout.addItem(self.endStretch)

    def createBottomBar(self, *args, **kwargs):
        # Layouts
        layout = qw.QHBoxLayout()

        # Elements
        addBt = qw.QPushButton("+")
        addBt.setFixedSize(60,17)
        
        # Functions
        addBt.setMenu(self.createAddMenu())
        # addBt.clicked.connect(self.addMenu)

        # Style
        layout.setContentsMargins(0,10,0,0)
        
        # Layout management
        layout.addStretch()
        layout.addWidget(addBt)
        layout.addStretch()

        return layout

    def createElements(self, *args, **kwargs):
        '''
        Create elements for the item display.
        '''
        # Creation
        self.colorBar = wd.ColorBar()
        self.colorBar.setFixedWidth(7)
        self.baseLayout_V.setContentsMargins(5, 0, 0, 0)
        self.centerColumn = qw.QVBoxLayout()
        self.contentLayout = qw.QVBoxLayout()
        self.bottomBar = self.createBottomBar()

        self.contentLayout.setContentsMargins(0, 0, 0, 0)
        self.contentLayout.setSpacing(5)
        self.titleText.setPlaceholderText("Title")


        # Layout Management
        self.mainLayout.addLayout(self.centerColumn, 1)
        self.baseLayout.insertWidget(0, self.colorBar)

        self.centerColumn.addLayout(self.contentLayout, 1)
        self.centerColumn.addLayout(self.bottomBar, 0)

    def setTitleVisibility(self, value=None, toggle=False):
        '''
        Set the visibility of the title QLineEdit. Needs a value or the toggle
        argument to be set.
        :bool value: Show or hide the title.
        :bool toggle: If True, will toggle the current visibility state.
        :None return:
        '''
        if value is None and not toggle:
            raise Exception("Value argument is not filled and the toggle argument is False.")
        elif toggle:
            value = self.titleText.isHidden()

        self.titleText.setHidden(not value) # Inverse bool

class Page(qw.QWidget):
    '''
    Page that holds a list of items.
    :param parent: Parent of the page.
    :param title: Title that will be displayed on the tab and on top of the page.
    '''
    def __init__(self, parent=None, title=None, *args, **kwargs):
        super(Page, self).__init__(parent=parent)
        self.title = title
        self.setStyleSheet(st.globalStyle)
        self.create()
        
        # Color
        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), qg.QColor(35, 35, 35))
        self.setPalette(p)

        # Shortcuts
        shortcutNew = qw.QShortcut(qg.QKeySequence("Ctrl+N"), self)
        shortcutNew.activated.connect(self.createItem)
        shortcutNew.setEnabled(True)

    def create(self):
        # Creation
        self.mainLayout = qw.QVBoxLayout(self)
        self.content = self.createContent()
        self.contentScroll = qw.QScrollArea()
        titleLayout = qw.QHBoxLayout()
        self.closeButton = wd.IconButton(iconFunction=ic.closeIcon, size=st.ButtonSize.large)
        self.titleLabel = wd.SingleLineEdit(text=self.title,
                                            placeHolderText="New Tab")
        # separator = wd.Separator()
        self.bottomRow = self.createBottomRow()

        # Function

        # Style
        self.titleLabel.setStyleSheet("font-size:20px")
        self.mainLayout.setContentsMargins(10, 5, 5, 0)
        self.contentScroll.setStyleSheet(st.scrollbar)
        self.contentScroll.setWidgetResizable(True)
        self.contentScroll.setFrameShape(qw.QFrame.NoFrame)

        # Layout management
        titleLayout.addWidget(self.titleLabel, 1)
        titleLayout.addWidget(self.closeButton, 0)
        self.contentScroll.setWidget(self.content)
        self.mainLayout.addLayout(titleLayout)
        # self.mainLayout.addWidget(separator)
        self.mainLayout.addWidget(self.contentScroll)
        self.mainLayout.addLayout(self.bottomRow)

    def createContent(self):
        # Creation
        content = qw.QWidget()
        self.contentLayout = qw.QVBoxLayout()
        self.contentStretch= qw.QSpacerItem(0,
                                            0,
                                            qw.QSizePolicy.Expanding,
                                            qw.QSizePolicy.Expanding)

        # Style
        self.contentLayout.setContentsMargins(0, 0, 0, 0)
        self.contentLayout.setSpacing(10)
        p = content.palette()
        p.setColor(content.backgroundRole(), qg.QColor(35,35,35))
        content.setPalette(p)

        # Layout Management
        content.setLayout(self.contentLayout)
        self.contentLayout.addItem(self.contentStretch)

        return content

    def createItem(self, title=None):
        item = Item()
        if title: item.titleText.setText(title)
        self.contentLayout.removeItem(self.contentStretch)
        self.contentLayout.addWidget(item)
        self.contentLayout.addItem(self.contentStretch)
        item.closeButton.clicked.connect(partial(self.deleteItem, item))

        return item

    def deleteItem(self, item):
        self.contentLayout.removeWidget(item)
        item.deleteLater()

    def createBottomRow(self):
        '''
        Bottom row with the add button.
        '''
        bottomRow = qw.QHBoxLayout()
        self.addItemBt = qw.QPushButton("+")

        # self.addItemBt.setMenu(self.menu)
        
        # Style
        self.addItemBt.setFixedSize(75,20)

        # Function
        self.addItemBt.clicked.connect(self.createItem)

        # Layout management
        bottomRow.addStretch()
        bottomRow.addWidget(self.addItemBt)
        bottomRow.addStretch()
        
        return bottomRow
    
    def getState(self):
        '''
        Loop over the content's layout and call each item's getState function.
        '''
        state = dict()
        state["title"] = self.titleLabel.text()
        itemData = list()
        for i in range(self.contentLayout.count()):
            item = self.contentLayout.itemAt(i).widget()
            if isinstance(item, Item):
                itemData.append(item.getState())
        state["items"] = itemData
        # print("PAGE STATE : ", state)
        return state

class MainWindow(qw.QWidget):
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent=parent)
        # Variables
        self.pages = None
        self.startResize = False
        self.startResizePos = qc.QPoint(0, 0)
        self.setWindowFlags(qc.Qt.FramelessWindowHint)
        self.setMouseTracking(True)
        self.undoQueue = list()

        # Creation
        self.layout = qw.QVBoxLayout()
        self.titleBar = Frame(self, title="Notes Today")
        self.setWindowTitle("Notes Today")
        self.tabs = qw.QTabWidget()
        self.tabs.setTabPosition(qw.QTabWidget.West)

        # Style
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)
        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), qg.QColor(20, 20, 20))
        self.setPalette(p)
        self.tabs.setStyleSheet(st.tabCSS)
        self.resize(350,700)

        # Layout Management
        self.setLayout(self.layout)
        self.layout.addWidget(self.titleBar)
        self.layout.addWidget(self.tabs)

        # Shortcuts
        shortcutNewPage = qw.QShortcut(qg.QKeySequence("Ctrl+Shift+N"), self)
        shortcutNewPage.activated.connect(self.newPage)
        shortcutNewPage.setEnabled(True)

        shortcutClose = qw.QShortcut(qg.QKeySequence("Ctrl+W"), self)
        shortcutClose.activated.connect(self.close)
        shortcutClose.setEnabled(True)

        self.loadSave()
        self.setGeometry(150, 150, 800, 800)

    def createMenu(self):
        self.mainMenu = self.menuBar()
        self.mainMenu.setSizePolicy(qw.QSizePolicy.Fixed, qw.QSizePolicy.Expanding)
        new = self.mainMenu.addMenu("New")
        new.addAction("Tab")
        new.addAction("PostIt")
        new.addAction("Element")
        pref = self.mainMenu.addMenu("Preferences")
        pref.addAction("Edit")
        pref.addAction("Check")
        self.mainMenu.addMenu("Help")
    
    def newPage(self, title=None, createStartItem=False):
        newTabTitle = title or "New Tab"
        page = Page(title=newTabTitle)
        self.tabs.addTab(page, newTabTitle)
        page.titleLabel.setText(newTabTitle)
        if createStartItem:
            page.createItem(title="First Item")
        # Function
        page.titleLabel.returnPressed.connect(partial(self.updateTabTitle, page))
        page.closeButton.clicked.connect(partial(self.closePage, page))

        return page

    def updateTabTitle(self, page, *args, **kwargs):
        index = self.tabs.indexOf(page)
        text = page.titleLabel.text()
        self.tabs.setTabText(index, text)

    def closePage(self, page):
        tab = self.tabs.indexOf(page)
        self.tabs.removeTab(tab)
        page.deleteLater()

    def closeEvent(self, event):
        super().closeEvent(event)
        self.saveContent()

    def saveContent(self):
        state = {
            "currentTab":self.tabs.currentIndex(),
            "tabs":list()
        }
        for i in range(self.tabs.count()):
            page = self.tabs.widget(i)
            # print("MAIN PAGE", page)
            state["tabs"].append(page.getState())
        # print(state)
        dt.saveData(state)

    def loadSave(self):
        self.savedData = dt.readSave()
        if self.savedData is None:
            return None
        # print("", self.savedData)
        if "tabs" in self.savedData:
            for page in self.savedData["tabs"]:
                newPage = self.newPage(title=page["title"])
                for itemData in page["items"]:
                    item = newPage.createItem(title=itemData["title"])
                    item.restoreState(itemData)
        
        if "currentTab" in self.savedData:
            self.tabs.setCurrentIndex(self.savedData["currentTab"])

    def mousePressEvent(self, event):
        rsZone = 5
        super().mousePressEvent(event)
        size = self.size()
        if (rsZone < event.x() < size.width()-rsZone
           and rsZone < event.y() < size.height()-rsZone):
           pass
        else:
            self.startResize = True
            self.startResizePos = self.mapToGlobal(event.pos())

    def mouseReleaseEvent(self, event):
        super().mouseReleaseEvent(event)
        self.startResize = False

    def mouseMoveEvent(self, event):
        super().mouseMoveEvent(event)
        self.isCursorOnSide(event.pos())
        if self.startResize:
            end = self.mapToGlobal(event.pos())
            movement = end - self.startResizePos
            windowSize = qc.QSize(self.width() + movement.x(), self.height() + movement.y())
            windowPos = self.pos()
            self.setGeometry(windowPos.x(),
                             windowPos.y(),
                             windowSize.width(),
                             windowSize.height())
            self.startResizePos = end

    def enterEvent(self, event):
        pass
    
    def leaveEvent(self, event):
        pass

    def isCursorOnSide(self, pos):
        pass
        # sideMargin = 8
        # position = ""
        # if (sideMargin < pos.x() < self.width()-sideMargin
        #     and sideMargin < pos.y() < self.height()-sideMargin):
        #     self.setCursor(qc.Qt.ArrowCursor)
        # else:
        #     if pos.x() < sideMargin:
        #         position += "L"
        #     if self.width()-sideMargin < pos.x():
        #         position += ("R")
        #     if pos.y() < sideMargin:
        #         position += ("T")
        #     if self.height()-sideMargin < pos.y():
        #         position += ("B")
            
        #     if position in ("L", "R"):
        #         self.setCursor(qc.Qt.SizeHorCursor)
        #     elif position in ("T", "B"):
        #         self.setCursor(qc.Qt.SizeVerCursor)
        #     elif position in ("LT", "RB"):
        #         self.setCursor(qc.Qt.SizeFDiagCursor)
        #     elif position in ("LB", "TR"):
        #         self.setCursor(qc.Qt.SizeBDiagCursor)

class Frame(qw.QWidget):
    def __init__(self, parent=None, title=None, *args, **kwargs):
        super().__init__()
        ICON_BASE_COLOR = qg.QColor(75, 75, 75)
        self.parent = parent
        self.parentSize = self.parent.size()
        self.parentPos = self.parent.pos()
        self.maximized = False

        self.pressing = False
        self.startMove = qc.QPoint(0, 0)
        title = title or "New Window"

        # UI Creation
        self.setStyleSheet(st.globalStyle)
        self.mainLayout = qw.QHBoxLayout()
        self.mainLayout.setContentsMargins(10, 0, 0, 0)
        self.mainLayout.setSpacing(0)
        self.setLayout(self.mainLayout)
        self.title = qw.QLabel(title)
        self.title.setStyleSheet("color:rgba(255,255,255,150);")

        # Icon
        self.icon = qw.QLabel()
        self.icon.setContentsMargins(0, 0, 5, 0)
        self.icon.setPixmap(ic.editIcon(size=qc.QSize(12, 12),
                                        color=qg.QColor(200, 200, 200)))

        # Close Button
        self.closeButton = wd.IconButton(iconFunction=ic.closeIcon, baseColor=ICON_BASE_COLOR)
        self.closeButton.clicked.connect(self.closeButtonClicked)
        self.closeButton.setFixedSize(40, 25)
        self.closeButton.setStyleSheet("QPushButton:hover{background-color:rgb(210,25,25);}")
        
        # Min Button
        self.minButton = wd.IconButton(iconFunction=ic.minimizeIcon, baseColor=ICON_BASE_COLOR)
        self.minButton.clicked.connect(self.minButtonClicked)
        self.minButton.setFixedSize(40, 25)
        self.minButton.setStyleSheet("QPushButton:hover{background-color:rgb(50,50,50);}")

        # Max Button
        self.maxButton = wd.IconButton(iconFunction=ic.maximizeIcon, baseColor=ICON_BASE_COLOR)
        self.maxButton.clicked.connect(self.maxButtonClicked)
        self.maxButton.setFixedSize(40, 25)
        self.maxButton.setStyleSheet("QPushButton:hover{background-color:rgb(50,50,50);}")

        self.mainLayout.addWidget(self.icon, 0)
        self.mainLayout.addWidget(self.title, 1)
        self.mainLayout.addWidget(self.minButton, 0)
        self.mainLayout.addWidget(self.maxButton, 0)
        self.mainLayout.addWidget(self.closeButton, 0)

    def mousePressEvent(self, event):
        self.startMove = self.mapToGlobal(event.pos())
        self.pressing = True

    def mouseMoveEvent(self, event):
        super().mouseMoveEvent(event)
        if self.pressing:
            if self.maximized:
                self.parent.showNormal()
            end = self.mapToGlobal(event.pos())
            movement = end - self.startMove
            self.parent.setGeometry(self.mapToGlobal(movement).x(),
                                    self.mapToGlobal(movement).y(),
                                    self.parent.width(),
                                    self.parent.height())
            self.startMove = end
    
    def mouseReleaseEvent(self, event):
        super().mouseReleaseEvent(event)
        self.pressing = False

    def closeButtonClicked(self):
        self.parent.close()

    def maxButtonClicked(self):
        if self.maximized is True:
            self.parent.showNormal()
            self.maximized = False
        else:
            self.parent.showMaximized()
            self.maximized = True
    
    def minButtonClicked(self):
        self.parent.showMinimized()

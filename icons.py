'''
Premade icons functions to get pixmap.
Includes:
  -Close Icon
  -Plus Icon
  -Maximize Icon
  -Minimize Icon
  -Pen Icon
  -Missing Icon
  -Progress Bar Icon
  -Text Icon
  -Check Icon
  -CheckList Icon
  -Move Icon
  -Menu Icon
'''

import PySide2.QtWidgets as qw
import PySide2.QtGui as qg
import PySide2.QtCore as qc

def iconCreation(func):
    '''
    Creates default icon arguments variables.
    '''
    def wrapper(*args, **kwargs):
        size = kwargs["size"] if "size" in kwargs else qc.QSize(32, 32)
        color = kwargs["color"] if "color" in kwargs else qg.QColor(25, 25, 25)
        margin = kwargs["margin"] if "margin" in kwargs else .1
        pixmap = kwargs["pixmap"] if "pixmap" in kwargs else qg.QPixmap(size)
        pixmap.fill(qg.Qt.transparent)
        painter = kwargs["painter"] if "painter" in kwargs else qg.QPainter(pixmap)

        return func(size=size,
                    color=color,
                    margin=margin,
                    pixmap=pixmap,
                    painter=painter)
    return wrapper

@iconCreation
def closeIcon(size=None, color=None, margin=None, painter=None, pixmap=None):
    '''
    Cross Icon for closing icons.
    :QSize size: Size of the icon.
    :QColor color: Color of the icon.
    :float margin: ratio of space around the icon (0 to 1).
    :return QPixmap: Icon.
    '''
    brush = qg.QBrush()
    brush.setColor(color)
    brush.setStyle(qg.Qt.SolidPattern)

    # Cross
    path = qg.QPainterPath()
    pathPoints = [
        qc.QPoint(size.width()*margin, size.height()*(margin+.1)),
        qc.QPoint(size.width()*(1.0-margin-.1), size.height()*(1.0-margin)),
        qc.QPoint(size.width()*(1.0-margin), size.height()*(1.0-margin-.1)),
        qc.QPoint(size.width()*(margin+.1), size.height()*margin),
    ]
    path.moveTo(pathPoints[0])
    for point in pathPoints[1:]:
        path.lineTo(point)
    painter.fillPath(path, brush)
    
    path = qg.QPainterPath()
    pathPoints = [
        qc.QPoint(size.width()*margin, size.height()*(1.0-margin-.1)),
        qc.QPoint(size.width()*(margin+.1), size.height()*(1.0-margin)),
        qc.QPoint(size.width()*(1.0-margin), size.height()*(margin+.1)),
        qc.QPoint(size.width()*(1.0-margin-.1), size.height()*margin),
    ]
    path.moveTo(pathPoints[0])
    for point in pathPoints[1:]:
        path.lineTo(point)
    painter.fillPath(path, brush)
    painter.end()

    return pixmap

@iconCreation
def maximizeIcon(size=None, color=None, margin=None, painter=None, pixmap=None):
    '''
    Docstring
    '''
    brush = qg.QBrush(qg.Qt.transparent, qg.Qt.SolidPattern)
    painter.setBrush(brush)
    pen = qg.QPen(color)
    pen.setWidth(size.width()*.12)
    painter.setPen(pen)

    offset = .18
    # Background square
    rect = qc.QRect(size.width()*(margin+offset),
                    size.height()*margin,
                    size.width()*(1.0-(2*margin)-offset),
                    size.height()*(1.0-(2*margin)-offset))
    painter.drawRect(rect)
    # Foreground square
    rect.translate(-size.width()*offset, size.height()*offset)
    painter.setCompositionMode(qg.QPainter.CompositionMode_Source)
    painter.fillRect(rect, qg.QColor(0,0,0,0))
    painter.setCompositionMode(qg.QPainter.CompositionMode_SourceOver)
    painter.drawRect(rect)

    painter.end()

    return pixmap

@iconCreation
def minimizeIcon(size=None, color=None, margin=None, painter=None, pixmap=None):
    brush = qg.QBrush()
    brush.setColor(color)
    brush.setStyle(qg.Qt.SolidPattern)

    height = size.height()*.15

    rect = qc.QRect(
        size.width()*margin,
        size.height()*.5,
        size.width()*(1.0-margin*2),
        height
    )
    painter.fillRect(rect, brush)

    return pixmap

@iconCreation
def missingIcon(size=None, color=None, margin=None, painter=None, pixmap=None):
    '''
    Missing image icon.
    :QSize size: size of the pixmap to be returned.
    :QColor color: color of the icon.
    :return QPixmap: pixmap containing the icon.
    '''
    pen = qg.QPen()
    pen.setColor(color)
    outlineWidth = 5
    pen.setWidth(outlineWidth)
    painter.setPen(pen)

    # Outline
    outlineRect = qc.QRect(0, 0, size.width(), size.height())
    painter.drawRect(outlineRect)

    # Sun
    brush = qg.QBrush()
    brush.setColor(color)
    brush.setStyle(qg.Qt.SolidPattern)
    painter.setBrush(brush)
    sunCenter = qc.QPoint(size.width()*.6, size.height()*.3)
    painter.drawEllipse(sunCenter, size.height()*.1, size.height()*.1)
    
    # Mountain One
    path = qg.QPainterPath()
    path.moveTo(qc.QPoint(size.width()*.1,
                          size.height()))
    path.lineTo(qc.QPoint(size.width()*.4,
                          size.height()*.4))
    path.lineTo(qc.QPoint(size.width()*.7,
                          size.height()))
    path.lineTo(qc.QPoint(size.width()*.1,
                          size.height()))
    painter.fillPath(path, brush)

    # Mountain Two
    path.translate(qc.QPoint(size.width()*.3,
                             size.height()*.15))
    painter.fillPath(path, brush)
    painter.end()

    return pixmap

def tempIcon(size=(64,64), color=(30,)*3):
    '''
    Cross Icon for closing icons.
    :param color: tuple(vector) color of the icon.
    :return QPixmap: Close Icon.
    '''
    pixmap = qg.QPixmap(*size)
    pixmap.fill(qg.Qt.transparent)
    painter = qg.QPainter(pixmap)
    brush = qg.QPen()
    brush.setColor(qg.QColor(*color))
    brush.setWidth(8)
    painter.setPen(brush)
    
    rect = qc.QRect(10,10, size[0]-20, size[1]-20)
    painter.drawRoundedRect(rect, 10, 10)

    painter.end()
    return pixmap

@iconCreation
def dragIcon(size=None, color=None, margin=None, painter=None, pixmap=None, horizontal=True):
    '''
    Cross Icon for closing icons.
    :param color: tuple(vector) color of the icon.
    :return QPixmap: Close Icon.
    '''
    brush = qg.QBrush()
    brush.setStyle(qg.Qt.SolidPattern)
    brush.setColor(color)
    painter.setBrush(brush)

    ratio = pixmap.height()/4.0
    for i in range(1,4):
        if horizontal:
            painter.drawEllipse(qc.QPoint(ratio*i, size.height()/2), 4,4)
        else:
            painter.drawEllipse(qc.QPoint(size.width()/2, ratio*i), 4,4)

    painter.end()
    return pixmap

@iconCreation
def editIcon(size=None, color=None, margin=None, painter=None, pixmap=None):
    brush = qg.QBrush()
    brush.setColor(color)
    brush.setStyle(qg.Qt.SolidPattern)
    painter.setBrush(brush)

    # Pen Path
    path = qg.QPainterPath()
    points = [
        qc.QPoint(size.width()*.05, size.height()), # Tip of the pen
        qc.QPoint(size.width()*.2, size.height()*.95),
        qc.QPoint(size.width()*.8, size.height()*.4),
        qc.QPoint(size.width()*.6, size.height()*.2),
        qc.QPoint(size.width()*.05, size.height()*.8),
    ]
    path.moveTo(points[0])
    for point in points[1:]:
        path.lineTo(point)
    painter.fillPath(path, brush)

    # Tip Path
    path = qg.QPainterPath()
    points = [
        qc.QPoint(size.width()*.9, size.height()*.3),
        qc.QPoint(size.width()*.95, size.height()*.2),
        qc.QPoint(size.width()*.8, size.width()*0.05),
        qc.QPoint(size.width()*.7, size.height()*.1),
    ]
    path.moveTo(points[0])
    for point in points[1:]:
        path.lineTo(point)
    painter.fillPath(path, brush)    

    painter.end()

    return pixmap

def squareIcon(size=None, color=None):
    size = size or qc.QSize(32, 32)
    color = color or  qg.QColor(25, 25 ,25)
    pixmap = qg.QPixmap(size)
    pixmap.fill(qg.Qt.transparent)
    painter = qg.QPainter(pixmap)

    pen = qg.QPen()
    pen.setWidth(6)
    pen.setColor(color)
    painter.setPen(pen)

    painter.drawRect(qc.QRect(0,0,size.width(), size.height()))

    painter.end()

    return pixmap

@iconCreation
def plusIcon(size=None, color=None, margin=None, painter=None, pixmap=None):
    pen = qg.QPen()
    pen.setWidth(size.width()*.1)
    pen.setColor(color)
    painter.setPen(pen)

    painter.drawLine(size.width()/2, size.height()*margin, size.width()/2, size.height()*(1.0-margin))
    painter.drawLine(size.width()*margin, size.height()*.5, size.width()*(1.0-margin), size.height()/2)

    painter.end()

    return pixmap

@iconCreation
def menuIcon(size=None, color=None, margin=None, painter=None, pixmap=None):
    brush = qg.QBrush(color, qc.Qt.BrushStyle.SolidPattern)

    lineHeight = size.height() * .15
    lineWidth = size.width() * (1.0-(margin*2))
    space = (size.height()-(lineHeight*3 + size.height()*margin*2))/2
    offset = space + lineHeight

    rect = qc.QRect(size.width() * margin, size.height() * margin, lineWidth, lineHeight)
    painter.fillRect(rect, brush)

    rect.translate(0, offset)
    painter.fillRect(rect, brush)

    rect.translate(0, offset)
    painter.fillRect(rect, brush)

    painter.end()

    return pixmap

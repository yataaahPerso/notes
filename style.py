'''
Contains styling for UI elements (colors and fonts).
'''
import sys
import PySide2.QtWidgets as qw
import PySide2.QtGui as qg
import PySide2.QtCore as qc

DEFAULT_COLOR = qg.QColor(25, 25, 25)
BACKGROUND_COLOR = qg.QColor(255,0,0)
TEXT_COLOR = None
ICON_BASECOLOR = None
ICON_HOVERCOLOR = None

colorPatterns = {
    "Dark":{
        "window_background":(25, 25, 25),
        "background-color":(25, 25, 25, 255),
        "text-color":(155, 155, 155),
        "icon":(70,70,70),
        "iconHover":(200, 200, 200)
    }
}

class ColorPattern():
    windowBackground = qg.QColor()
    backgroundColor = qg.QColor()
    iconColor = qg.QColor()
    iconHoverColor = qg.QColor()
    textColor = qg.QColor()

    # Highlight color
    red = qg.QColor(220, 30, 30)
    orange = qg.QColor(255, 125, 0)
    yellow = qg.QColor(255, 200, 0)
    green = qg.QColor(150, 230, 50)
    cyan = qg.QColor(10, 230, 190)
    blue = qg.QColor(0, 125 , 230)
    purple = qg.QColor(150, 90, 230)
    white = qg.QColor(255, 255, 255)
    black = qg.QColor(0, 0, 0)

class ButtonSize(object):
    small = qc.QSize(16, 16)
    normal = qc.QSize(22, 22)
    large = qc.QSize(32, 32)
    huge = qc.QSize(64, 64)

class HighlightColor(object):
    red = qg.QColor(220, 30, 30)
    orange = qg.QColor(255, 125, 0)
    yellow = qg.QColor(255, 200, 0)
    green = qg.QColor(150, 230, 50)
    cyan = qg.QColor(10, 230, 190)
    blue = qg.QColor(0, 125 , 230)
    purple = qg.QColor(150, 90, 230)
    white = qg.QColor(255, 255, 255)
    black = qg.QColor(0, 0, 0)

textEditCSS = """
background-color:rgba(10,10,10,0);
border:none;
color:white;
"""

tabCSS = '''
QTabBar::tab{
    background:rgb(35,35,35);
    color:white;
    border-top-left-radius:4px;
    border-bottom-left-radius:4px;
}
QTabBar::tab:selected{
    padding: 20px 0px 20px 0px;
}
QTabBar::tab:!selected{
    color:grey;
    padding: 20px 0px 20px 0px;
    border-top-left-radius:0px;
    background:rgb(27,27,27);
}
QTabWidget::pane{
    border:none;
}

'''

scrollbar = '''
'''

globalStyle = """
QLineEdit, QPlainTextEdit, QTextEdit {
    background-color:rgba(10,10,10,0);
    text-align:right;
    border:none;
    color:white;
}
QPushButton{
    background-color:rgba(255,255,255,0);
    border:none;
    color:black;
    font-size:14px;
}
QMenu{
    background-color:rgb(45,45,45);
    color:white;
}
QMenu::item:selected{
    background-color:rgba(255,125,0, 200);
    color:white;
}
QPushButton:hover{
    background-color:rgba(255,255,255,40);
    border:none;
    color:black;
}
 QPushButton::menu-indicator{width:0px;}
 QPushButton:hover:pressed{
    background-color:rgba(255,255,255,100);
    border:none;
    color:white;
}
"""

mainWindow = """
background-color:rgba(15,15,15,255);
"""

whiteText = """
color:white;
font-size:11px;
"""

def iconCreation(func):
    '''
    Creates default icon arguments variables.
    '''
    def wrapper(*args, **kwargs):
        size = kwargs["size"] if "size" in kwargs else qc.QSize(32, 32)
        color = kwargs["color"] if "color" in kwargs else qg.QColor(25, 25, 25)
        margin = kwargs["margin"] if "margin" in kwargs else .1
        pixmap = kwargs["pixmap"] if "pixmap" in kwargs else qg.QPixmap(size)
        pixmap.fill(qg.Qt.transparent)
        painter = kwargs["painter"] if "painter" in kwargs else qg.QPainter(pixmap)

        return func(size=size,
                    color=color,
                    margin=margin,
                    pixmap=pixmap,
                    painter=painter)
    return wrapper

@iconCreation
def closeIcon(size=None, color=None, margin=None, painter=None, pixmap=None):
    '''
    Cross Icon for closing icons.
    :QSize size: Size of the icon.
    :QColor color: Color of the icon.
    :float margin: ratio of space around the icon (0 to 1).
    :return QPixmap: Icon.
    '''
    brush = qg.QBrush()
    brush.setColor(color)
    brush.setStyle(qg.Qt.SolidPattern)

    # Cross
    path = qg.QPainterPath()
    pathPoints = [
        qc.QPoint(size.width()*margin, size.height()*(margin+.1)),
        qc.QPoint(size.width()*(1.0-margin-.1), size.height()*(1.0-margin)),
        qc.QPoint(size.width()*(1.0-margin), size.height()*(1.0-margin-.1)),
        qc.QPoint(size.width()*(margin+.1), size.height()*margin),
    ]
    path.moveTo(pathPoints[0])
    for point in pathPoints[1:]:
        path.lineTo(point)
    painter.fillPath(path, brush)
    
    path = qg.QPainterPath()
    pathPoints = [
        qc.QPoint(size.width()*margin, size.height()*(1.0-margin-.1)),
        qc.QPoint(size.width()*(margin+.1), size.height()*(1.0-margin)),
        qc.QPoint(size.width()*(1.0-margin), size.height()*(margin+.1)),
        qc.QPoint(size.width()*(1.0-margin-.1), size.height()*margin),
    ]
    path.moveTo(pathPoints[0])
    for point in pathPoints[1:]:
        path.lineTo(point)
    painter.fillPath(path, brush)
    painter.end()

    return pixmap

@iconCreation
def maximizeIcon(size=None, color=None, margin=None, painter=None, pixmap=None):
    '''
    Docstring
    '''
    brush = qg.QBrush(qg.Qt.transparent, qg.Qt.SolidPattern)
    painter.setBrush(brush)
    pen = qg.QPen(color)
    pen.setWidth(size.width()*.12)
    painter.setPen(pen)

    offset = .18
    # Background square
    rect = qc.QRect(size.width()*(margin+offset),
                    size.height()*margin,
                    size.width()*(1.0-(2*margin)-offset),
                    size.height()*(1.0-(2*margin)-offset))
    painter.drawRect(rect)
    # Foreground square
    rect.translate(-size.width()*offset, size.height()*offset)
    painter.setCompositionMode(qg.QPainter.CompositionMode_Source)
    painter.fillRect(rect, qg.QColor(0,0,0,0))
    painter.setCompositionMode(qg.QPainter.CompositionMode_SourceOver)
    painter.drawRect(rect)

    painter.end()

    return pixmap

@iconCreation
def minimizeIcon(size=None, color=None, margin=None, painter=None, pixmap=None):
    brush = qg.QBrush()
    brush.setColor(color)
    brush.setStyle(qg.Qt.SolidPattern)

    height = size.height()*.15

    rect = qc.QRect(
        size.width()*margin,
        size.height()*.5,
        size.width()*(1.0-margin*2),
        height
    )
    painter.fillRect(rect, brush)

    return pixmap

def missingIcon(size=None, color=None):
    '''
    Missing image icon.
    :QSize size: size of the pixmap to be returned.
    :QColor color: color of the icon.
    :return QPixmap: pixmap containing the icon.
    '''
    size = size or qc.QSize(256, 160)
    color = color or qg.QColor(25, 25 ,25)
    pixmap = qg.QPixmap(size)
    pixmap.fill(qg.Qt.transparent)

    painter = qg.QPainter(pixmap)
    pen = qg.QPen()
    pen.setColor(color)
    outlineWidth = 5
    pen.setWidth(outlineWidth)
    painter.setPen(pen)

    # Outline
    outlineRect = qc.QRect(0, 0, size.width(), size.height())
    painter.drawRect(outlineRect)

    # Sun
    brush = qg.QBrush()
    brush.setColor(color)
    brush.setStyle(qg.Qt.SolidPattern)
    painter.setBrush(brush)
    sunCenter = qc.QPoint(size.width()*.6, size.height()*.3)
    painter.drawEllipse(sunCenter, size.height()*.1, size.height()*.1)
    
    # Mountain One
    path = qg.QPainterPath()
    path.moveTo(qc.QPoint(size.width()*.1,
                          size.height()))
    path.lineTo(qc.QPoint(size.width()*.4,
                          size.height()*.4))
    path.lineTo(qc.QPoint(size.width()*.7,
                          size.height()))
    path.lineTo(qc.QPoint(size.width()*.1,
                          size.height()))
    painter.fillPath(path, brush)

    # Mountain Two
    path.translate(qc.QPoint(size.width()*.3,
                             size.height()*.15))
    painter.fillPath(path, brush)
    painter.end()

    return pixmap

def tempIcon(size=(64,64), color=(30,)*3):
    '''
    Cross Icon for closing icons.
    :param color: tuple(vector) color of the icon.
    :return QPixmap: Close Icon.
    '''
    pixmap = qg.QPixmap(*size)
    pixmap.fill(qg.Qt.transparent)
    painter = qg.QPainter(pixmap)
    brush = qg.QPen()
    brush.setColor(qg.QColor(*color))
    brush.setWidth(8)
    painter.setPen(brush)
    
    rect = qc.QRect(10,10, size[0]-20, size[1]-20)
    painter.drawRoundedRect(rect, 10, 10)

    painter.end()
    return pixmap

def dragIcon(horizontal=False, color=(155,155,155)):
    '''
    Cross Icon for closing icons.
    :param color: tuple(vector) color of the icon.
    :return QPixmap: Close Icon.
    '''
    pixmap = qg.QPixmap(64,64)
    pixmap.fill(qg.Qt.transparent)
    painter = qg.QPainter(pixmap)
    pen = qg.QPen()
    pen.setWidth(5)
    pen.setColor(qg.QColor(*color))
    painter.setPen(pen)

    ratio = pixmap.height()/4.0
    for i in range(1,4):
        if horizontal:
            painter.drawEllipse(qc.QPoint(ratio*i, 32), 5,5)
        else:
            painter.drawEllipse(qc.QPoint(32, ratio*i), 5,5)


    painter.end()
    return pixmap

@iconCreation
def editIcon(size=None, color=None, margin=None, painter=None, pixmap=None):
    brush = qg.QBrush()
    brush.setColor(color)
    brush.setStyle(qg.Qt.SolidPattern)
    painter.setBrush(brush)

    # Pen Path
    path = qg.QPainterPath()
    points = [
        qc.QPoint(size.width()*.05, size.height()), # Tip of the pen
        qc.QPoint(size.width()*.2, size.height()*.95),
        qc.QPoint(size.width()*.8, size.height()*.4),
        qc.QPoint(size.width()*.6, size.height()*.2),
        qc.QPoint(size.width()*.05, size.height()*.8),
    ]
    path.moveTo(points[0])
    for point in points[1:]:
        path.lineTo(point)
    painter.fillPath(path, brush)

    # Tip Path
    path = qg.QPainterPath()
    points = [
        qc.QPoint(size.width()*.9, size.height()*.3),
        qc.QPoint(size.width()*.95, size.height()*.2),
        qc.QPoint(size.width()*.8, size.width()*0.05),
        qc.QPoint(size.width()*.7, size.height()*.1),
    ]
    path.moveTo(points[0])
    for point in points[1:]:
        path.lineTo(point)
    painter.fillPath(path, brush)    

    painter.end()

    return pixmap

def squareIcon(size=None, color=None):
    size = size or qc.QSize(32, 32)
    color = color or  qg.QColor(25, 25 ,25)
    pixmap = qg.QPixmap(size)
    pixmap.fill(qg.Qt.transparent)
    painter = qg.QPainter(pixmap)

    pen = qg.QPen()
    pen.setWidth(6)
    pen.setColor(color)
    painter.setPen(pen)

    painter.drawRect(qc.QRect(0,0,size.width(), size.height()))

    painter.end()

    return pixmap

@iconCreation
def addIcon(size=None, color=None, margin=None, painter=None, pixmap=None):
    pass

@iconCreation
def menuIcon(size=None, color=None, margin=None, painter=None, pixmap=None):
    brush = qg.QBrush(color, qc.Qt.BrushStyle.SolidPattern)

    lineHeight = size.height() * .15
    lineWidth = size.width() * (1.0-(margin*2))
    space = (size.height()-(lineHeight*3 + size.height()*margin*2))/2
    offset = space + lineHeight

    rect = qc.QRect(size.width() * margin, size.height() * margin, lineWidth, lineHeight)
    painter.fillRect(rect, brush)

    rect.translate(0, offset)
    painter.fillRect(rect, brush)

    rect.translate(0, offset)
    painter.fillRect(rect, brush)

    painter.end()

    return pixmap

# def windowSize():
#     desktop = qw.QDesktopWidget()
#     return desktop.availableGeometry().center()

if __name__ == "__main__":
    app = qw.QApplication()

    a = qw.QWidget()
    la = qw.QVBoxLayout()
    la.setContentsMargins(0,0,0,0)
    lab = qw.QLabel()
    lab.setPixmap(missingIcon())
    la.addWidget(lab)
    a.setLayout(la)
    a.show()

    sys.exit(app.exec_())
    
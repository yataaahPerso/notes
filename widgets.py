'''
Contains widgets elements.
'''
import sys
sys.path.append("E:/DEV")
import PySide2.QtWidgets as qw
import PySide2.QtGui as qg
import PySide2.QtCore as qc
import notes.style as st
import notes.icons as ic
from functools import partial

def blank(*args, **kwargs):
    print("Placeholder function.", kwargs)

class BaseTag(qw.QWidget):
    def __init__(self, parent=None, title=None, backgroundColor=None):
        super().__init__(parent=parent)
        self.setMinimumSize(50, 15)
        self.title = title or "Default"
        self.backgroundColor = backgroundColor

    def paintEvent(self, event):
        painter = qg.QPainter(self)
        painter.setRenderHint(qg.QPainter.Antialiasing, True)

        bgRect = qc.QRect(0,
                          0,
                          painter.device().width(),
                          painter.device().height())
        pen = qg.QPen(qg.QColor(255, 255, 255, 0), 1)
        path = qg.QPainterPath()
        path.addRoundedRect(bgRect, 3, 3)
        painter.setPen(pen)
        painter.fillPath(path, self.backgroundColor)
        # painter.fillRect(bgRect, qg.QColor(210, 25, 25))

        pen.setColor(qg.QColor(255, 255, 255))
        painter.setPen(pen)
        font = qg.QFont("Arial", 7)
        font.setBold(True)
        painter.setFont(font)
        painter.drawText(bgRect, qc.Qt.AlignCenter, self.title)

        painter.end()

class CheckBox(qw.QCheckBox):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

    def paintEvent(self, event):
        painter = qg.QPainter(self)
        # Background
        bgRect = qc.QRect(0,
                          0,
                          painter.device().height(),
                          painter.device().height())
        painter.fillRect(bgRect, st.DEFAULT_COLOR)

        # Checked
        checkSize = 3
        if self.isChecked():
            rect = qc.QRect(checkSize,
                            checkSize,
                            painter.device().height()-checkSize*2,
                            painter.device().height()-checkSize*2)
            painter.fillRect(rect, qg.QColor(200, 200, 200))
        
        painter.end()

class QHBoxLayout(qw.QHBoxLayout):
    def __init__(self, parent=None, spacing=0, contentsMargins=(0,0,0,0)):
        super().__init__(parent=parent)
        self.setContentsMargins(*contentsMargins)
        self.setSpacing(spacing)

class QVBoxLayout(qw.QVBoxLayout):
    def __init__(self, parent=None, spacing=0, contentsMargins=(0,0,0,0)):
        super().__init__(parent=parent)
        self.setContentsMargins(*contentsMargins)
        self.setSpacing(spacing)

class FloatingImage(qw.QLabel):
    def __init__(self, pixmap=None):
        super().__init__()
        if pixmap:
            self.setPixmap(pixmap)
        else:
            self.setPixmap(ic.missingIcon())
        self.setWindowFlags(qc.Qt.WindowFlags(qc.Qt.FramelessWindowHint))

class BaseWidget(qw.QWidget):
    def __init__(self,
                 parent=None,
                 collapsable=False,
                 collapsed=False,
                 hasOptionMenu=True,
                 restore=None
                 ):
        super().__init__(parent=parent)
        # Object Variables
        self.collapsable = collapsable
        self.hasOptionMenu = hasOptionMenu
        self.startClicked = False
        self.clickedMove = None
        
        self.baseLayout = QHBoxLayout()
        self.baseLayout_V = QVBoxLayout()
        self.mainLayout = QVBoxLayout()
        self._contentWidget = qw.QWidget()
        self._contentWidget.setLayout(self.mainLayout)
        # self._contentWidget.setAutoFillBackground(True)
        self.titleBarLayout = self.createTitleBar()
        self.setTopButtonHidden(True)

        self.baseLayout.addLayout(self.baseLayout_V)
        self.baseLayout_V.addLayout(self.titleBarLayout, 0)
        self.baseLayout_V.addWidget(self._contentWidget, 1)
        self.baseLayout_V.addStretch()
        self.setLayout(self.baseLayout)

        if hasOptionMenu is False:
            self.menuButton.setInvisible(True)
        if collapsed:
            self._contentWidget.setHidden(True)

        # Create the widget from restore state
        if restore and isinstance(restore, dict):
            self.restoreState(restore)

    def createTitleBar(self):
        titleLayout = QHBoxLayout()
        self.titleText = SingleLineEdit(placeHolderText="Title")
        self.titleText.setRetainSpace(True)
        self.collapseButton = IconButton(
            toggleButton=True,
            iconFunction=ic.minimizeIcon,
            toggleIconFunction=ic.plusIcon)
        self.collapseButton.clicked.connect(partial(self.setCollapse, toggle=True))
        # Hide and lock
        if self.collapsable is False:
            self.collapseButton.setInvisible(True)
            self.collapseButton.lockVisibility = True

        self.closeButton = IconButton(iconFunction=ic.closeIcon)
        self.menuButton = IconButton(iconFunction=ic.menuIcon)
        self.menuButton.setMenu(self.createOptionMenu())

        titleLayout.addWidget(self.collapseButton, 0)
        titleLayout.addWidget(self.titleText, 1)
        titleLayout.addWidget(self.menuButton, 0)
        titleLayout.addWidget(self.closeButton, 0)

        return titleLayout

    def enterEvent(self, event):
        # Show the buttons in the title bar
        self.setTopButtonHidden(False)

    def leaveEvent(self, event):
        # Hide the buttons in the title bar
        self.setTopButtonHidden(True)

    def setTopButtonHidden(self, value):
        self.closeButton.setHidden(value)
        if self.hasOptionMenu:
            self.menuButton.setHidden(value)

    def createOptionMenu(self):

        menu = qw.QMenu(self)
        menu.addAction("Duplicate", blank)
        return menu

    def getState(self):
        state = dict()
        state["title"] = self.titleText.text()
        state["titleVisibility"] = self.titleText.isHidden()
        state["collapsable"] = self.collapsable
        state["collapsed"] = self._contentWidget.isHidden()
        state["hasOptionMenu"] = self.hasOptionMenu

        return state

    def restoreState(self, state):
        self.titleText.setText(state["title"])
        self.titleText.setHidden(state["titleVisibility"])
        self.setCollapse(value=state["collapsed"])
        if state["collapsable"] is False:
            self.collapseButton.setInvisible(True)
            self.collapseButton.lockVisibility = True

    def setCollapse(self, value=None, toggle=False):
        '''
        Set the visibility of the content widget. Needs a value or the toggle
        argument to be set.
        :bool value: Show or hide the title.
        :bool toggle: If True, will toggle the current visibility state.
        :None return:
        '''
        value = self.setHiddenOf(self._contentWidget, value=value, toggle=toggle)
        if value:
            self.collapseButton.setIcon

    def screenshot(self):
        '''
        Creates a screenshot of the widget and returns it as a QPixmap.
        '''
        screenshot = self.screen()

        return screenshot

    def setHiddenOf(self, widget, value=None, toggle=False):
        '''
        Set the visibility of the content widget. Needs a value or the toggle
        argument to be set.
        :bool value: Show or hide the title.
        :bool toggle: If True, will toggle the current visibility state.
        :bool return: State of visibility (True if hidden, False if visible)
        '''
        if value is None and toggle is False:
            raise Exception("Value argument is not filled and the toggle argument is False."
                            "Arguments were value={}, toggle={}".format(value, toggle))
        elif toggle:
            value = not widget.isHidden() # Inverse bool

        widget.setHidden(value)
        return value

class MoveButton(qw.QPushButton):
    def __init__(self, parent=None, ):
        super().__init__(parent=parent)
        self.setAutoRepeat(True)
        self.setAutoRepeatDelay(1000)
        self.setAutoRepeatInterval(1000)
        self.clicked.connect(self.moveClicked)
        self._state = 0

    def moveClicked(self):
        # If held down
        if self.isDown():
            # Toggle to held down after inital press
            if self._state == 0:
                self._state = 1
                self.setAutoRepeatInterval(50)
                print('press')
            else:
                print('repeat')
        # if not held down but was
        elif self._state == 1:
            self._state = 0
            self.setAutoRepeatInterval(1000)
            print('release')
        else:
            print('click')

class NewItem(BaseWidget):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        button = qw.QPushButton("Yolo")
        self.layout.addWidget(button)

class Tag(qw.QPushButton):
    def __init__(self, parent=None, name=None, color=None, *args, **kwargs):
        super().__init__(parent=parent)
        self.setText(name or ("New Tag"))
        self.setStyleSheet("border-radius=5px; background-color:red;margin-right:10px")
        self.setSizePolicy(qw.QSizePolicy.Minimum, qw.QSizePolicy.Minimum)

class TagBar(qw.QWidget):
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent=parent)
        self.mainLayout = QHBoxLayout()
        self.addTag()
        self.mainLayout.addStretch()
        self.setLayout(self.mainLayout)

    def addTag(self, tagName=None, color=None):
        tag = Tag(name=tagName, color=color)
        self.mainLayout.addWidget(tag)

class IconButton(qw.QPushButton):
    def __init__(self,
                 parent=None,
                 iconFunction=None,
                 toggleButton=False,
                 toggleIconFunction=None,
                 toggleState=False,
                 size=st.ButtonSize.normal,
                 baseColor=qg.QColor(25, 25, 25), 
                 hoverColor=qg.QColor(200, 200, 200),
                 *args,
                 **kwargs):
        '''
        Creates a QPushButton with icons. By default, retain its size when hidden.
        :QSize size: Minimum size of the button.
        :QPixmap icon: Base icon for the button.
        :QPixmap hoverIcon: Icon displayed on hover.
        '''
        super(IconButton, self).__init__(parent=parent)
        self.toggleButton = toggleButton
        self.toggleState = toggleState
        self.lockVisibility = False
        sizePolicy = self.sizePolicy()
        sizePolicy.setRetainSizeWhenHidden(True)
        self.setSizePolicy(sizePolicy)

        if size:
            self.setMinimumSize(size)
        if iconFunction is None:
            iconFunction = ic.missingIcon
        if toggleIconFunction is None:
            toggleIconFunction = ic.missingIcon
        if size is None:
            size = st.ButtonSize.normal
        
        # Base icons
        self.icon = iconFunction(
            size=size,
            color=baseColor)
        self.hoverIcon = iconFunction(
            size=size,
            color=hoverColor)
        # Toggle mode icons
        if toggleButton:
            self.toggleIcon = toggleIconFunction(
                size=size,
                color=baseColor)
            self.toggleIconHover = toggleIconFunction(
                size=size,
                color=hoverColor)

        self.setIcon(self.icon)
        self.setStyleSheet("QPushButton:hover{background-color:rgba(255,255,255,40)}")
        if toggleButton:
            self.clicked.connect(self.setToggleState)

    def setToggleState(self):
        self.toggleState = not self.toggleState
        self.enterEvent("") # Update the icon

    def setInvisible(self, value):
        sizePolicy = self.sizePolicy()
        sizePolicy.setRetainSizeWhenHidden(False)
        self.setSizePolicy(sizePolicy)
        self.setHidden(value)

    def setHidden(self, value):
        if self.lockVisibility is False:
            super().setHidden(value)

    def enterEvent(self, event):

        if self.toggleState:
            self.setIcon(self.toggleIconHover)
        else:
            self.setIcon(self.hoverIcon)
    
    def leaveEvent(self, event):
        if self.toggleState:
            self.setIcon(self.toggleIcon)
        else:
            self.setIcon(self.icon)

class DragButton(qw.QPushButton):
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent=parent)
        self.setIcon(st.dragIcon(horizontal=True))

    def parentScreenShot(self):
        pass

    def mousePressEvent(self, event):
        pass

    def mouseReleaseEvent(self, event):
        pass

    def mouseMoveEvent(self, event):
        pass

class SingleLineEdit(qw.QLineEdit):
    def __init__(self,
                 parent=None,
                 text=None,
                 placeHolderText="Text",
                 *args,
                 **kwargs):
        super(SingleLineEdit, self).__init__(parent=parent)
        self.inputText = text
        self.setPlaceholderText(placeHolderText)
        if text : self.setText(text)

    def setRetainSpace(self, value):
        sizePolicy = self.sizePolicy()
        sizePolicy.setRetainSizeWhenHidden(value)
        self.setSizePolicy(sizePolicy)
        
class Separator(qw.QFrame):
    def __init__(self, parent=None, *args, **kwargs):
        super(Separator, self).__init__(parent=parent)
        self.setFrameShape(qw.QFrame.HLine)

class ColorBar(qw.QWidget):
    def __init__(self, parent=None, color="orange", *args, **kwargs):
        super(ColorBar, self).__init__(parent=parent)
        self.setAutoFillBackground(True)
        self.menu = self.createColorMenu()
        self.setColor(color)

    def mousePressEvent(self, event):
        if event.button() == qc.Qt.RightButton:
            self.menu.popup(qg.QCursor.pos())

    def createColorMenu(self):
        menu = qw.QMenu(self)

        menu.addAction("Red", partial(self.setColor, "red"))
        menu.addAction("Orange", partial(self.setColor, "orange"))
        menu.addAction("Yellow", partial(self.setColor, "yellow"))
        menu.addAction("Green", partial(self.setColor, "green"))
        menu.addAction("Cyan", partial(self.setColor, "cyan"))
        menu.addAction("Blue", partial(self.setColor, "blue"))
        menu.addAction("Purple", partial(self.setColor, "purple"))
        menu.addAction("White", partial(self.setColor, "white"))
        menu.addAction("Black", partial(self.setColor, "black"))

        return menu

    def setColor(self, color):
        self.color = color
        print(color)
        color = getattr(st.HighlightColor, color)
        p = self.palette()
        p.setColor(self.backgroundRole(), color)
        self.setPalette(p)

class CheckListItem(qw.QWidget):
    def __init__(self,
                 parent=None,
                 title=None,
                 checked=False,
                 hasProgressBar=False,
                 progress=0,
                 placeHolderText="Enter Task...",
                 *args,
                 **kwargs):
        super(CheckListItem, self).__init__(parent=parent)
        # Variables
        self.title = title
        self.checked = checked
        self.hasProgressBar = hasProgressBar

        # Creation
        self.baseLayout = QVBoxLayout()
        self.baseLayout.setContentsMargins(10, 0, 0, 0)
        self.mainLayout = QHBoxLayout()
        self.checkBox = CheckBox()
        self.titleText = SingleLineEdit(text=self.title,
                                        placeHolderText=placeHolderText)
        self.addMenu = IconButton(iconFunction=ic.plusIcon)
        self.deleteButton = IconButton(iconFunction=ic.closeIcon)

        # Function
        self.checkBox.setChecked(self.checked)
        self.addMenu.setMenu(self.createAddMenu())
        self.checkBox.released.connect(self.checkToggle)

        # Style
        self.addMenu.setVisible(False)
        self.deleteButton.setVisible(False)
        
        # Progress bar
        self.progressWidget = qw.QWidget()
        progressLayout = QHBoxLayout()
        self.progressBar = _ProgBar()
        self.progressBar.setProgress(progress)
        self.progressBar.sliderMoved.connect(self.updateProgressBar)
        self.progressText = SingleLineEdit()
        self.progressText.setText("{} %".format(progress))
        self.progressText.setFixedWidth(35)
        progressLayout.addWidget(self.progressBar, 1)
        progressLayout.addWidget(self.progressText, 0)
        progressLayout.setContentsMargins(50, 0, 5, 0)
        progressLayout.setSpacing(10)
        self.progressWidget.setLayout(progressLayout)

        if hasProgressBar is False:
            self.progressWidget.setHidden(True)

        # Layout Management
        self.mainLayout.addWidget(self.checkBox, 0)
        self.mainLayout.addWidget(self.titleText, 1)
        self.mainLayout.addWidget(self.addMenu, 0)
        self.mainLayout.addWidget(self.deleteButton, 0)
        self.setLayout(self.baseLayout)
        self.baseLayout.addLayout(self.mainLayout)
        self.baseLayout.addWidget(self.progressWidget)
    
    def setProgressBarVisibility(self, value=None, toggle=False):
        if value is None and not toggle:
            raise Exception("Value argument is not filled and the toggle argument is False.")
        elif toggle:
            value = self.progressWidget.isHidden()
        value = not value # Invert

        self.progressWidget.setHidden(value)

    def updateProgressBar(self):
        percent = self.progressBar.value()
        self.progressText.setText("{}%".format(percent))

    def createAddMenu(self):
        menu = qw.QMenu(self)
        menu.addAction("Progress Bar", partial(self.setProgressBarVisibility, toggle=True))
        menu.addAction("Sub Checklist", blank)
        menu.addAction("Due Date", blank)
        menu.addAction("Done Date", blank)
        menu.addAction("Image", blank)
        menu.addAction("Reminder", blank)

        return menu

    def checkToggle(self):
        self.checked = self.checkBox.isChecked()

    def enterEvent(self, event):
        super().enterEvent(event)
        self.setAutoFillBackground(True)
        self.addMenu.setVisible(True)
        self.deleteButton.setVisible(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), qg.QColor(255, 255, 255, 20))
        self.setPalette(p)

    def leaveEvent(self, event):
        super().leaveEvent(event)
        self.addMenu.setVisible(False)
        self.deleteButton.setVisible(False)
        self.setAutoFillBackground(False)

    def getState(self):
        state = dict()
        state["title"] = self.titleText.text()
        state["checked"] = self.checkBox.isChecked()
        state["progressBarHidden"] = self.progressWidget.isHidden()
        state["progress"] = self.progressBar.value()

        return state

    def restoreState(self, data):
        self.titleText.setText(data["title"])
        self.checkBox.setChecked(data["checked"])
        self.checked = data["checked"]
        self.progressWidget.setHidden(data["progressBarHidden"])
        self.progressBar.setProgress(data["progress"])
        self.progressText.setText("{} %".format(data["progress"]))

class CheckList(BaseWidget):
    def __init__(self, parent=None, title=None, *args, **kwargs):
        super().__init__(parent=parent, collapsable=True)
        # Variables
        self.completed = 0
        self.taskCount = 0
        self.showProgressBar = True
        self.title = title
        
        # Title bar
        self.titleText.setPlaceholderText("Checklist")
        self.menuButton.setMenu(self.createToolMenu())
        progressLayout = QHBoxLayout()
        self.progressPercentage = qw.QLabel("0 %")
        self.progressText = qw.QLabel("0/0")
        self.progressBar = _ProgBar()
        self.entry = self.taskInputWidget()

        # Function
        progressLayout.setContentsMargins(5, 0, 5, 0)
        progressLayout.setSpacing(10)
        self.inputText.returnPressed.connect(self.newTaskFromInput)
        self.progressBar.setEnabled(False)

        # Style
        self.setStyleSheet(st.textEditCSS)
        self.progressPercentage.setMinimumSize(35, 20)

        # Layout Management

        progressLayout.addWidget(self.progressText)
        progressLayout.addWidget(self.progressBar)
        progressLayout.addWidget(self.progressPercentage)
        self.mainLayout.addLayout(progressLayout)
        self.mainLayout.addLayout(self.entry)

        self.setProgressBarVisibility(value=self.showProgressBar)

    def updateTaskCount(self):
        count = 0
        completed = 0
        for i in range(self.mainLayout.count()):
            widget = self.mainLayout.itemAt(i).widget()
            if isinstance(widget, CheckListItem):
                count += 1
                completed += widget.checked
        self.taskCount = count
        self.completed = completed

    def updateProgressBar(self):
        self.updateTaskCount()
        if self.taskCount > 0:
            progress = int(self.completed/self.taskCount*100)
        else:
            progress = 0
        self.progressBar.setProgress(progress)
        self.progressPercentage.setText(str(progress)+" %")
        self.progressText.setText("{}/{}".format(self.completed, self.taskCount))

    def taskInputWidget(self):
        '''
        Create the textEdit at the bottom of the list for the user to add tasks.
        '''
        layout = QHBoxLayout()
        layout.setSpacing(0)
        space = qw.QWidget() # Is used in place of a check box
        space.setFixedSize(20,20)
        self.inputText = SingleLineEdit(placeHolderText="New Task...")
        layout.addWidget(space, 0)
        layout.addWidget(self.inputText, 1)
        self.inputLayout = layout

        return layout

    def newTaskFromInput(self):
        taskTitle = self.inputText.text()
        if taskTitle:
            self.createTask(title=taskTitle)
            # Move temp task to bottom and reset it
            self.mainLayout.removeItem(self.inputLayout)
            self.mainLayout.addLayout(self.inputLayout)
            self.inputText.setText("")
            self.setStyleSheet(st.textEditCSS)
            self.updateProgressBar()

    def createTask(self, title=None, value=False, hasProgressBar=False, progress=0):
        task = CheckListItem(title=title,
                             checked=value,
                             hasProgressBar=hasProgressBar,
                             progress=progress)
        self.mainLayout.insertWidget(self.mainLayout.count()-1, task)
        # Remove entry layout to place it at the end
        # self.mainLayout.removeItem(self.entry)
        # self.mainLayout.addLayout(self.entry)
        task.deleteButton.clicked.connect(partial(self.deleteTask, task))
        task.checkBox.clicked.connect(self.updateProgressBar)
        return task

    def deleteTask(self, task):
        self.mainLayout.removeWidget(task)
        task.deleteLater()
        self.updateProgressBar()

    def createToolMenu(self):
        menu = qw.QMenu(self)
        menu.addAction("Toggle Title", partial(self.setHiddenOf, self.titleText,  toggle=True))
        menu.addAction("Toggle Progress Bar", partial(self.setProgressBarVisibility, toggle=True))

        return menu

    def setProgressBarVisibility(self, value=None, toggle=False):
        if value is None and not toggle:
            raise Exception("Value argument is not filled and the toggle argument is False.")
        elif toggle:
            value = self.progressBar.isHidden()
        value = not value # Invert

        self.progressText.setHidden(value)
        self.progressBar.setHidden(value)
        self.progressPercentage.setHidden(value)

    def getState(self):
        '''
        Collect self infos and tasks' info and return them as a dictionnary.
        '''
        state = super().getState()
        state["type"] = "CheckList"
        state["progressVisibility"] = not self.progressBar.isHidden()

        # Gather Checklist Items state
        tasks = list()
        for i in range(self.mainLayout.count()):
            item = self.mainLayout.itemAt(i).widget()
            if isinstance(item, CheckListItem):
                tasks.append(item.getState())
        state["tasks"] = tasks

        return state

    def restoreState(self, data):
        super().restoreState(data)
        for task in data["tasks"]:
            newTask = self.createTask()
            newTask.restoreState(task)
        # Checklist global progress bar
        self.setProgressBarVisibility(value=data["progressVisibility"])
        self.updateProgressBar()

class Image(qw.QWidget):
    def __init__(self, parent=None, imageFile=None, *args, **kwargs):
        super().__init__(parent=parent)
        self.imageFile = imageFile
        
        self.layout = QHBoxLayout()
        sideLayout = QVBoxLayout()
        self.closeButton = IconButton(icon=ic.closeIcon(),
                                      hoverIcon=ic.closeIcon(color=qg.QColor(150,150,150)))
        self.image = qw.QLabel()

        # Style
        self.closeButton.setFixedSize(20, 20)
        sideLayout.setContentsMargins(0, 0, 0, 0)
        self.layout.setContentsMargins(0, 0, 0, 0)
        if self.imageFile:
            self.image.setPixmap(qg.QPixmap(self.imageFile))
        else:
            self.image.setPixmap(ic.missingIcon(size=qc.QSize(96, 64)))
        
        # Layout Management
        self.setLayout(self.layout)
        sideLayout.addWidget(self.closeButton, 0)
        sideLayout.addStretch()
        self.layout.addWidget(self.image, 1)
        self.layout.addLayout(sideLayout, 0)

    def getState(self):
        state = {"imageFile":self.imageFile,
                 "type":"Image"}

        return state

class Gallery(qw.QWidget):
    def __init__(self, parent=None, *args, **kwargs):
        super(Gallery, self).__init__(parent=parent)

    def saveState(self):
        state = dict()
        state["type"] = "gallery"
        state["images"] = self.images

        return state

class Text(BaseWidget):
    def __init__(self, parent=None, text=None, *args, **kwargs):
        super(Text, self).__init__(parent=parent, collapsable=True)
        self.textEditor = qw.QTextEdit()
        self.setAutoFillBackground(False)
        self.mainLayout.addWidget(self.textEditor)

        self.textEditor.document().contentsChanged.connect(self.updateSizeFromText)
        self.textEditor.setPlaceholderText("Type your text here...")
        if text:
            self.textEditor.setPlainText(text)

    def resizeEvent(self, event):
        super().resizeEvent(event)
        self.updateSizeFromText()

    def sizeHint(self):
        return qc.QSize(20,25)

    def updateSizeFromText(self):
        docHeight = self.textEditor.document().size().height()
        self.setMinimumHeight(docHeight)

    def getState(self):
        state = {
            "type":"Text",
            "text":self.textEditor.toPlainText()
        }

        return state

    def restoreState(self, data):
        self.textEditor.setText(data["text"])
        self.updateSizeFromText()

class ProgressBar(BaseWidget):
    def __init__(self, parent=None, title="Title", progress=0, *args, **kwargs):
        '''
        Creates a progress bar widget.
        :string title: Title displayed above the progress bar.
        :int progress: Progress displayed (between 0 and 100)
        '''
        super().__init__(parent=parent, hasOptionMenu=False)
        # Variables
        self.title = title
        self.progress = progress
        # Style
        self.setSizePolicy(qw.QSizePolicy.Expanding, qw.QSizePolicy.Minimum)
        self.create()
                
    def create(self):
        # Create
        self.secondLayout = QHBoxLayout()
        self.progressPercentage = qw.QLineEdit("{}%".format(self.progress))
        self.progressPercentage.setFixedWidth(35)
        self.progressBar = _ProgBar(progress=self.progress)

        # Style
        self.progressPercentage.setStyleSheet(st.textEditCSS)
        self.progressPercentage.setAlignment(qc.Qt.AlignRight)
        
        # Functions
        self.progressBar.sliderMoved.connect(self.setProgress)
        self.progressPercentage.returnPressed.connect(self.progressChanged)

        # Layout Management
        self.secondLayout.addWidget(self.progressBar)
        self.secondLayout.addWidget(self.progressPercentage)
        self.mainLayout.addLayout(self.secondLayout)
        self.mainLayout.addStretch()

    def setProgress(self, value):
        self.progress = value
        self.progressPercentage.setText(str(value)+"%")
        self.progressChanged()

    def progressChanged(self):
        text = self.progressPercentage.text()
        value = int("".join([a for a in text if a.isnumeric()]))
        if value < 0:
            value = 0
            self.progressPercentage.setText("0%")
        elif value > 100:
            value = 100
            self.progressPercentage.setText("100%")
        else:
            self.progressPercentage.setText("{}%".format(value))

        self.progressBar.setProgress(value)

    def changeTitle(self, value):
        self.title = self.titleText.text()

    def getState(self):
        state = {
            "title":self.titleText.text(),
            "type":"ProgressBar",
            "value":self.progress
        }
        return state

    def restoreState(self, data):
        self.title = data["title"]
        self.titleText.setText(self.title)
        self.setProgress(data["value"])

class ProgressBarList():
    pass

class _ProgBar(qw.QAbstractSlider):
    '''
    Creates a progress bar widget.
    :int progress: Progress displayed (between 0 and 100)
    '''
    def __init__(self, parent=None, progress=0, *args, **kwargs):
        super(_ProgBar, self).__init__(parent=parent)
        self.setSizePolicy(qw.QSizePolicy.Expanding, qw.QSizePolicy.Expanding)
        self.setFixedHeight(6)
        self.setMinimum(0)
        self.setMaximum(100)
        self.progressRatio = progress / 100.0

        # Style
        self.backgroundColor = qg.QColor(25, 25, 25)
        self.mainColor = qg.QColor(210, 210, 210)

    def setProgress(self, progress):
        self.progressRatio = progress / 100.0
        self.setValue(progress)
        self.update()

    def paintEvent(self, e):
        painter = qg.QPainter(self)
        # Background bar
        bgRect = qc.QRect(0,
                          0,
                          painter.device().width(),
                          painter.device().height())
        painter.fillRect(bgRect, self.backgroundColor)
        # Progress bar
        rect = qc.QRect(0,
                        0,
                        painter.device().width() * self.progressRatio,
                        painter.device().height())
        painter.fillRect(rect, self.mainColor)
        painter.end()

    def updateFromClick(self, e):
        if e.pos().x() > self.width():
            self.progressRatio = 1
        else:
            self.progressRatio = (e.pos().x())/self.width()
        flt = 100 * self.progressRatio
        value = int(flt)

        self.setValue(value)
        self.sliderMoved.emit(value)
        self.update()

    def mousePressEvent(self, e):
        self.pressed = True
        self.updateFromClick(e)
    
    def mouseReleaseEvent(self, e):
        self.pressed = False

    def mouseMoveEvent(self, e):
        if self.pressed:
            self.updateFromClick(e)


'''
This module prodive function to work with data from saves.
(functionToCall, args)
'''
import os, json

APPDATA_DIR = os.getenv("APPDATA")
SAVE_FILE = os.path.join(APPDATA_DIR, "NotesToday_userSave.json")

def saveExists():
    return os.path.isfile(SAVE_FILE)

def readSave():
    if saveExists() is False:
        return None

    with open(SAVE_FILE, "r") as f:
        save = json.load(f)
    return save

def saveData(data):
    with open(SAVE_FILE, "w+") as f:
        json.dump(data, f, indent=4)